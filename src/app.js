import {isValid, enableButton, calcVideoHeight, addListeners, switchPlayerVisibility} from './helpers.js'
import './styles.css'

// Несколько запасных ключей, чтобы не париться о превышении квоты :)
const api_key = 'AIzaSyA2CWPbc5gpeoeRzVUZJLKSHlqAm76hVyg'
// const api_key = 'AIzaSyCnrtoFJVKfSWDPn8DRjyAAHvlAicQkVk8'
// const api_key = 'AIzaSyB5xqlmqtse7ji0ZFs5Q0hrU5PdnX2UT-U'
// const api_key = 'AIzaSyCRmL_XYZPamAqrl5lB6Q2azz-Zq5FHQPk'

const input = document.getElementById('search-input')
const submitButton = document.getElementById('submit-button')
const whatWeSearch = document.getElementById('field-info')
const list = document.getElementById('to-render')

submitButton.addEventListener('click', fetchVideos)
input.addEventListener('input', () => enableButton(input, submitButton))

let videosArray = []
let arrayToRender = []

function fetchVideos() {
	showLoader(true)
	// Делаю запрос видео по ключевому слову и сохраняю в массив
	return fetch(`https://youtube.googleapis.com/youtube/v3/search?part=snippet&maxResults=10&order=date&q=${input.value}&key=${api_key}`)
		.then(response => response.json())
		.then(response => response.items.map(item => {
			if (item.id.videoId) {
				videosArray.push({
					id: item.id.videoId,
					title: item.snippet.title,
					channelTitle: item.snippet.channelTitle,
					description: item.snippet.description || '',
					publishTime: item.snippet.publishTime,
					image: item.snippet.thumbnails.medium.url,
					viewCount: ''
				})
			}
		}))
		.then(fetchViewCounts)
		.then(renderList)
		.then(addClickListenersForOpenPlayer)
		.then(() => showLoader(false))
		.catch(err => console.log(err))		
}

// Перебираю массив с видео и для каждого делаю запрос о количестве просмотров, объединяю информацию и делаю сортировку
function fetchViewCounts() {	
	if (videosArray) {
		return Promise.all(videosArray.map(item => {
			return fetch(`https://youtube.googleapis.com/youtube/v3/videos?&part=statistics&id=${item.id}&maxResults=10&key=${api_key}`)
			.then(response => response.json())
			.then(data => {
				item.viewCount = +data.items[0].statistics.viewCount
				arrayToRender.push(item)
				arrayToRender.sort((a,b) => a.viewCount < b.viewCount ? 1 : -1)
			})
		}))
	}
}

// Перебираю и вывожу каждый элемент массива, формирую вывод ключевого слова по которому сделан поиск
function renderList(content) {

	const html = arrayToRender.length
		? arrayToRender.map(toCard).join('')
		: `<div class=" text-center text-danger pb-2">Здесь появятся результаты поиска</div>`

	list.innerHTML = html

	if (content) {
		let whatWeSearchValue = input.value
		let infoAboutSearch = `<div class=" text-center text-muted"> Результат поиска по запросу <span class="text-danger">${whatWeSearchValue}</span></div>`
		whatWeSearch.classList.remove('invisible')
		whatWeSearch.innerHTML = infoAboutSearch
	}
}

// Формирую карточку для каждого видео, вычисляю высоту плеера в зависимости от ширины контейнера
function toCard(video) {
	return `
		<div class="row item-card mt-2">
			<div class="d-flex align-items-center flex-column">
				<img class="img m-1 cursor-pointer" src="${video.image}" alt="">
				<span class="text-muted">${(video.publishTime).slice(0, 10)}</span>
			</div>
			<div class="col">
				<h5 class="text-primary cursor-pointer title">${video.title}</h5>
				<span class="text-danger mr-2">${video.channelTitle}</span>
				<p class="cursor-pointer">${video.description}</p>
			</div>
			<iframe class="video hidden" width="100%" height="${calcVideoHeight()}"
			src="https://www.youtube.com/embed/${video.id}"
			frameborder="0"
			allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
			allowfullscreen
		>
		</iframe>
		</div>

		<hr>
	`
}

// Функция показа и скрытия анимации загрузки, заодно блокирую кнопку поиска пока идет запрос
function showLoader(isLoaderVisisble) {
	if (isLoaderVisisble) {
		document.querySelector('.loader').classList.remove('invisible')
	} else {
		document.querySelector('.loader').classList.add('invisible')
		whatWeSearch.classList.remove('invisible')
		resetInputAndVariables()
	}
	submitButton.disabled = isLoaderVisisble
}

// Очищение инпута и переменных для следующего поиска
function resetInputAndVariables() {
	input.value = ''
	videosArray = []
	arrayToRender = []
}

// Добавление слушатели клика для открытия плеера к заголовкам и картинкам превью
function addClickListenersForOpenPlayer() {
	addListeners(document.querySelectorAll('.img'))
	addListeners(document.querySelectorAll('.title'))
}

renderList()