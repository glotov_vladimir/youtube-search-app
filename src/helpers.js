// Простая валиадция на минимум два символа в инпуте
export function isValid(value) {
	return value.length >= 2
}

// Переключение активности кнопки в зависимости от валидности ключевого слова
export function enableButton(input, submitButton) {
	isValid(input.value) ? submitButton.disabled = false : submitButton.disabled = true
}

// Вычисление высоты плеера
export function calcVideoHeight() {
	let container = document.querySelector('.container')
	return (container.offsetWidth / 1.778)
}

// Переключение видимости плеера
export function switchPlayerVisibility(i) {
	let videos = document.querySelectorAll('.video')
	videos[i].classList.toggle('hidden')
}

// Добавление слушателей клика к элементам
export function addListeners(elem) {
	elem.forEach((item, i) => item.addEventListener('click', () => switchPlayerVisibility(i)))
}